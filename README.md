# My project's README

## Requirements
* Mozilla Firefox 51.0.1

Python modules:

* selenium 3.02
* tldextract 2.0.2
* urllib3 1.13.1

## Usage
Collect HARs:
``` 
./command.sh
```

Extract csv:
```
./extract.py -i /foo/bar/har_folder/ -o /baz/output_folder/ -t tracker_list_20170125
```