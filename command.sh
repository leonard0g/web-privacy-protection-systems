#!/bin/sh
PROFILES="$1"	#/home/giannantoni/.mozilla/firefox
URLS="$2" 		#./urls
OUT=$3 			#./round1
ROUNDS=$4		#10
BACKOFF=$5 		#10

if [ "$#" -ne 5 ]; then
    echo "Illegal number of parameters"
    echo "Usage: " $0 " profiles_path urls_path out_path rounds backoff"
    exit
fi

for f in $(ls $URLS);
do
	echo $URLS/$f;
	for i in $(seq 1 $ROUNDS);
    do
    	echo "ROUND " $i
	    for profile in $(ls -d $PROFILES/*/ | grep -Ev "default|Crash|*.ini");
		do
        	echo $profile
        	#echo $URLS/$f
        	#echo $OUT/$(basename $profile )/${i}_${f}
            ./WebTrafficGenerator/web_traffic_generator.py $URLS/$f $OUT/$(basename $profile )/${i}_${f} -p $profile -b $BACKOFF -v;
        done;
    done;
done;

