The dataset contains the results obtained by performing 16,000 visits (8 browser profiles * 100 websites * 10 repetitions * 2 [with or without cookie consent]).
Each line of the file reports the following attributes:

1. **profile**. The browser profile employed for this visit. This can be Plain (no tracker blocker installed) or one of the 7 tracker blockers we test in our paper (AdBlock Plus, Ghostery, Blur, uBlock, Request Policy, Disconnect, Privacy Badger)
2. **cookie**. Whether consent to Cookie Policy is given or not 
3. **category**. Website Category
4. **run**. ID of the run. There are 10 different runs (repetitions) for each visit
5. **domain**. URL of the website visited
6. **byte_index**. Byte Index obtained on this visit (definition of this metric is given in [1])
7. **object_index**. Object Index obtained on this visit (definition of this metric is given in [1])
8. **on_load_time**. Time in seconds for the browser to trigger the OnLoad event
9. **tot_bytes**. Total number of bytes dowloaded to build the page
10. **tot_objects**. Total number of objects downloaded to build the page
11. **tot_3rd_party**. Total number of requests to third-party domains
12. **unique_3rd_party**. Number of unique third-party domains contacted
13. **tot_trackers**. Total number of requests to trackers
14. **unique_trackers**. Number of unique third-party domains contacted
15. **trackers**. List where each element reports a tracker domain contacted during the visit and corresponding number of requests generated for that domain
16. **3rd_party**. List where each element reports a third-party domain contacted during the visit  and corresponding number of requests generated for that domain

[1] Bocchi et Al, Measuring the Quality of Experience of Web users, Internet-QoE �16, http://dl.acm.org/citation.cfm?id=2940138
