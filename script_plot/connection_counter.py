import numpy as np
from collections import defaultdict


matrix = []

with open("../data/data_sonda1.csv", "r") as csv:
	for line in csv:
		if "byte_index" not in line:
			lineArray = line.split(",")
			matrix.append(lineArray)

csv.close()

plugins = ['Plain', 'Ghostery', 'Privacy Badger', 'AdBlock Plus', 'Disconnect', 'Blur', 'uBlock', 'Request Policy']

#### Distinct trackers

# res = {}
# for plugin in plugins:
# 	connections_to_trackers = []
# 	for array in matrix:
# 		if array[0] == plugin and array[1] == "n":
# 			connections_to_trackers.append(int(array[13]))
# 	res[plugin] = np.median(connections_to_trackers)

# for plugin in sorted(plugins):
# 	print(plugin, 1-float(res[plugin])/float(res["Plain"]))

### Loading time

# res = {}
# for plugin in plugins:
# 	loading_time = []
# 	for array in matrix:
# 		if array[0] == plugin and array[1] == "y":
# 			loading_time.append(float(array[7]))
# 	res[plugin] = np.mean(loading_time)

# for plugin in sorted(plugins):
# 	print(plugin, 1-float(res[plugin])/float(res["Plain"]))

#### Bandwidth

# res = {}
# for plugin in plugins:
# 	loading_time = []
# 	for array in matrix:
# 		if array[0] == plugin and array[1] == "y":
# 			loading_time.append(float(array[8]))
# 	res[plugin] = np.median(loading_time)

# for plugin in sorted(plugins):
# 	print(plugin, 1-float(res[plugin])/float(res["Plain"]))


res = {}
pages = set()
for plugin in ['Plain', 'Privacy Badger', 'uBlock']:
	loading_time = defaultdict(list)
	for array in matrix:
		if array[0] == plugin and array[1] == "y":
			loading_time[array[4]].append(float(array[7]))
			pages.add(array[4])
	res[plugin] = {x:np.mean(loading_time[x]) for x in loading_time.keys()}

# print(res)

for page in pages:
	try:
		if res["Plain"][page] > res["Privacy Badger"][page]:
			print("%s\t%f\t%f\t%f" % (page, res["Plain"][page], res["uBlock"][page], res["Privacy Badger"][page]))
	except KeyError:
		print(page)