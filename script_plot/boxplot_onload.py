#!/usr/bin/python3

import os
import sys
import argparse
import glob
import pandas as pd
import seaborn as sns
from collections import defaultdict
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='blablabla')
parser.add_argument('input', metavar='input', type=str, help='Input csv file.') 
parser.add_argument('output', metavar='output', type=str, help='Output folder.') 

args = parser.parse_args()
csv_path = os.path.abspath(args.input)
out_dir = os.path.abspath(args.output)

if not out_dir[0] == "/":
	out_dir = os.getcwd() + "/" + out_dir  

if not os.path.exists(out_dir):
	os.makedirs(out_dir)

title = "on-load-"
base_profile = "Plain"
profiles = ['Plain', 'Ghostery', 'Privacy Badger', 'AdBlock Plus', 'Disconnect', 'Blur', 'uBlock', 'Request Policy']
categories = ['Motors', 'E-Commerce', 'Forums', 'Games', 'Hobbies', 'News', 'Search Engines', 'Sport', 'Technology', 'Weather Forecast']
categories_palette = ["#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a"]
cookie_palette = ["#1f78b4", "#ff4c4c"] #"#b2df8a"
rotation = 45


# csv format
# "profile", "cookie", category", "run", "domain", "byte_index", "object_index", "on_load_time", "tot_bytes",
# "tot_objects", "tot_3rd_party", "unique_3rd_party", "tot_trackers", "unique_trackers", "trackers", "3rd_party"

df = pd.read_csv(open(csv_path))
df.replace(to_replace={'cookie': {'n': 'Without TP cookies setup', 'y': 'With TP cookies setup'}}, inplace=True)

# sort by median
df2 = df.groupby(['profile', 'cookie']).median().reset_index().sort_values(by='on_load_time', ascending=False)
df2 = df2[['profile', 'cookie']]
profile_order_no_cookie = df2[df2['cookie']=='Without TP cookies setup'].profile.tolist()
profile_order_no_cookie = [p for p in profile_order_no_cookie if p != base_profile]
profile_order_no_cookie.insert(0, base_profile)

#sns.set_context("paper")
sns.set(font_scale=1.7)
sns.set_style('ticks', {'font.family':'serif', 'font.serif':'Times New Roman'}) #sns.set_style("white")


# load time per profile, cooke; absolute values
# boxplot
bp_abs_cookie = sns.factorplot(kind='box',        # Boxplot
			   x='profile',       				# X-axis - first factor
			   y='on_load_time',   				# Y-axis - values for boxplot
			   hue='cookie',      				# Second factor denoted by color
			   hue_order=['Without TP cookies setup', 'With TP cookies setup'],
			   order=profile_order_no_cookie,
			   data=df,        					# Dataframe 
			   showfliers=False,
			   size=6,            				# Figure size (x100px)      
			   aspect=1.75,  					# Width = size * aspect 
			   palette=cookie_palette, #"muted" # Color palette
			   legend_out=False)  				# Make legend inside the plot
bp_abs_cookie.set_xticklabels(rotation=rotation)

bp_abs_cookie.set(ylim=(0,12))
bp_abs_cookie.despine(offset=10, trim=False)
bp_abs_cookie.set_xticklabels(profile_order_no_cookie, rotation=45, ha="right")
bp_abs_cookie.fig.get_axes()[0].legend(title= '',loc='upper right')
bp_abs_cookie.set_axis_labels('','Loading time [s]')


# load time per profile, category; absolute values
# boxplot
bp_abs_cat = sns.factorplot(kind='box',        	# Boxplot
			   x='profile',       				# X-axis - first factor
			   y='on_load_time',   				# Y-axis - values for boxplot
			   hue='category',      			# Second factor denoted by color
			   hue_order=categories,
			   order=profile_order_no_cookie,
			   palette=categories_palette, 		#"muted" # Color palette
			   data=df,        					# Dataframe 
			   showfliers=False,
			   size=6,            				# Figure size (x100px)      
			   aspect=3.0,  					# Width = size * aspect 
			   legend_out=False)  				# Make legend inside the plot
bp_abs_cat.set_xticklabels(rotation=rotation)

bp_abs_cat.set(ylim=(0,25))
bp_abs_cat.despine(offset=10, trim=False)
bp_abs_cat.set_xticklabels(profile_order_no_cookie, rotation=45, ha="right")
bp_abs_cat.fig.get_axes()[0].legend(title= '',loc='best', ncol=3)
bp_abs_cat.set_axis_labels('','Loading time [s]')


violin_abs_cookie = sns.factorplot(kind='violin',
					x='profile', y='on_load_time', hue='cookie',
					hue_order=['Without TP cookies setup', 'With TP cookies setup'],
					order=profile_order_no_cookie,
                    palette=cookie_palette, #"muted" # Color palette
					split=True, data=df, jitter=True,
					size=6,            			      
			   	  	aspect=3.0,      			
			   	   	legend_out=False)
violin_abs_cookie.set_xticklabels(rotation=rotation)

violin_abs_cookie.set(ylim=(0,20))
violin_abs_cookie.despine(offset=10, trim=False)
violin_abs_cookie.set_xticklabels(profile_order_no_cookie, rotation=45, ha="right")
violin_abs_cookie.fig.get_axes()[0].legend(title= '',loc='upper right')
violin_abs_cookie.set_axis_labels('','Loading time [s]')

#sns.plt.show()

# Save figures
bp_abs_cookie.savefig(os.path.join(out_dir, title + "box-plot.svg"), bbox_inches='tight')
bp_abs_cookie.savefig(os.path.join(out_dir, title + "box-plot.pdf"), bbox_inches='tight')

bp_abs_cat.savefig(os.path.join(out_dir, title + "category-box-plot.svg"), bbox_inches='tight')
bp_abs_cat.savefig(os.path.join(out_dir, title + "category-box-plot.pdf"), bbox_inches='tight')

violin_abs_cookie.savefig(os.path.join(out_dir, title + "violin.svg"), bbox_inches='tight')
violin_abs_cookie.savefig(os.path.join(out_dir, title + "violin.pdf"), bbox_inches='tight')