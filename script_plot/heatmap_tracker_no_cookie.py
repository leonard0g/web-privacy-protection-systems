#!/usr/bin/python3

import os
import sys
import argparse
import glob
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from collections import defaultdict

parser = argparse.ArgumentParser(description='blablabla')
parser.add_argument('input', metavar='input', type=str, help='Input csv file.') 
parser.add_argument('output', metavar='output', type=str, help='Output folder.') 

args = parser.parse_args()
csv_path = os.path.abspath(args.input)
out_dir = os.path.abspath(args.output)

if not out_dir[0] == "/":
	out_dir = os.getcwd() + "/" + out_dir  

if not os.path.exists(out_dir):
	os.makedirs(out_dir)

title = "trackers-no-cookie-heatmap"
base_profile = "Plain"
profiles = ['Plain', 'Ghostery', 'Privacy Badger', 'AdBlock Plus', 'Disconnect', 'Blur', 'uBlock', 'Request Policy']
categories = ['Motors', 'E-Commerce', 'Forums', 'Games', 'Hobbies', 'News', 'Search Engines', 'Sport', 'Technology', 'Weather Forecast']
categories_palette = ["#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a"]
cookie_palette = ["#1f78b4", "#ff4c4c"] #"#b2df8a"
rotation = 45

# csv format
# "profile", "cookie", category", "run", "domain", "byte_index", "object_index", "on_load_time", "tot_bytes",
# "tot_objects", "tot_3rd_party", "unique_3rd_party", "tot_trackers", "unique_trackers", "trackers", "3rd_party"

df = pd.read_csv(open(csv_path))
df = df[df.cookie == 'n']
df.replace(to_replace={'cookie': {'n': 'Without TP cookies setup', 'y': 'With TP cookies setup'}}, inplace=True)

plt.figure(figsize=(7, 4))
#sns.set_context("paper")
sns.set(font_scale=1.3)
sns.set_style('white', {'font.family':'serif', 'font.serif':'Times New Roman'}) #sns.set_style("white")


# HEATMAP TRACKER
grouped = df.groupby(['profile', 'category']).mean().reset_index()
# get categories ordered by performance with base profile
#ordered_categories = list(grouped[grouped.profile==base_profile][['category', 'unique_trackers']].sort_values(by='unique_trackers', ascending=False).category)
# get profiles (but the base one) ordered by performance on first category
#ordered_profiles = list(grouped.sort_values(by='unique_trackers', ascending=False)[(grouped.category==ordered_categories[0]) & (grouped.profile!=base_profile)].profile)
#ordered_profiles.insert(0, base_profile)

ordered_profiles = list(df.groupby(['profile']).mean().reset_index().sort_values(by='unique_trackers', ascending=False).profile)
ordered_profiles = [p for p in ordered_profiles if p != base_profile]
ordered_profiles.insert(0, base_profile)

ordered_categories = list(df.groupby(['category']).mean().reset_index().sort_values(by='unique_trackers', ascending=False).category)

grouped['cat'] = grouped['category'].astype('category', categories=ordered_categories)
df2 = grouped.pivot("cat", "profile", "unique_trackers")
df2 = df2.reindex_axis(ordered_profiles, axis=1) # order dataframe by profiles performance
#print(df2)
#print(ordered_categories)
fig, ax = plt.subplots()
hmap = sns.heatmap(df2,
					annot=True,
					cbar=True,
					cmap="Blues", #cmap='viridis'
					linewidths=0.5,
					robust=True,
					) 

plt.xticks(rotation=45, ha='right')
plt.yticks(rotation=0)
hmap.set_ylabel('')
hmap.set_xlabel('')
cbar_ax = fig.axes[-1]
cbar_ax.set_ylabel('#unique trackers (average)')

hmap.figure.savefig(os.path.join(out_dir, title + ".svg"), bbox_inches='tight')
hmap.figure.savefig(os.path.join(out_dir, title + ".pdf"), bbox_inches='tight')
