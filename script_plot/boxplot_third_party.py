#!/usr/bin/python3

import os
import sys
import argparse
import glob
import pandas as pd
import seaborn as sns
from collections import defaultdict

parser = argparse.ArgumentParser(description='blablabla')
parser.add_argument('input', metavar='input', type=str, help='Input csv file.') 
parser.add_argument('output', metavar='output', type=str, help='Output folder.') 

args = parser.parse_args()
csv_path = os.path.abspath(args.input)
out_dir = os.path.abspath(args.output)

if not out_dir[0] == "/":
	out_dir = os.getcwd() + "/" + out_dir  

if not os.path.exists(out_dir):
	os.makedirs(out_dir)

title = "third-party-box-plot"
base_profile = "Plain"
categories = ['Motors', 'E-Commerce', 'Forums', 'Games', 'Hobbies', 'News', 'Search Engines', 'Sport', 'Technology', 'Weather Forecast']
categories_palette = ["#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a"]
cookie_palette = ["#1f78b4", "#ff4c4c"] #"#b2df8a"
rotation = 45
ymax = 60

# csv format
# "profile", "cookie", category", "run", "domain", "byte_index", "object_index", "on_load_time", "tot_bytes",
# "tot_objects", "tot_3rd_party", "unique_3rd_party", "tot_trackers", "unique_trackers", "trackers", "3rd_party"

df = pd.read_csv(open(csv_path))
df.replace(to_replace={'cookie': {'n': 'Without TP cookies setup', 'y': 'With TP cookies setup'}}, inplace=True)

#sns.set_context("paper")
sns.set(font_scale=1.7)
sns.set_style('ticks', {'font.family':'serif', 'font.serif':'Times New Roman'}) #sns.set_style("white")
whis=[5, 95] #whiskers length for boxplots


# BOXPLOT THIRD PARTY
# unique third party per profile; absolute values
# sort by median
df2 = df.groupby(['profile', 'cookie']).median().reset_index().sort_values(by='unique_3rd_party', ascending=False)
df2 = df2[['profile', 'cookie']]
profile_order_no_cookie = df2[df2['cookie']=='Without TP cookies setup'].profile.tolist()
profile_order_no_cookie = [p for p in profile_order_no_cookie if p != base_profile]
profile_order_no_cookie.insert(0, base_profile)

# 
bp_tp_abs = sns.factorplot(kind='box',        # Boxplot
			   x='profile',       				# X-axis - first factor
			   y='unique_3rd_party',   			# Y-axis - values for boxplot
			   hue='cookie',      				# Second factor denoted by color
			   hue_order=['Without TP cookies setup', 'With TP cookies setup'],
			   order=profile_order_no_cookie,
			   data=df,        					# Dataframe 
			   showfliers=False,				# show outliers
			   whis=whis,
			   size=6,            				# Figure size (x100px)      
			   aspect=1.75,  					# Width = size * aspect 
			   palette=cookie_palette, 			#"muted" # Color palette
			   legend_out=False)  				# Make legend inside the plot

#bp_tp_abs.set(ylim=(0, df.unique_3rd_party.max())
bp_tp_abs.set(ylim=(0,ymax))
bp_tp_abs.despine(offset=10, trim=False)
bp_tp_abs.set_xticklabels(profile_order_no_cookie, rotation=45, ha="right")
bp_tp_abs.fig.get_axes()[0].legend(title= '',loc='upper right')
bp_tp_abs.set_axis_labels('','Unique third parties')

#sns.plt.show()

bp_tp_abs.savefig(os.path.join(out_dir, title + ".svg"), bbox_inches='tight')
bp_tp_abs.savefig(os.path.join(out_dir, title + ".pdf"), bbox_inches='tight')

