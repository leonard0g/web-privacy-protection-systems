#!/usr/bin/python3

import os
import sys
import argparse
import glob
import pandas as pd
import seaborn as sns
from collections import defaultdict
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='blablabla')
parser.add_argument('input', metavar='input', type=str, help='Input csv file.') 
parser.add_argument('output', metavar='output', type=str, help='Output folder.') 

args = parser.parse_args()
csv_path = os.path.abspath(args.input)
out_dir = os.path.abspath(args.output)

if not out_dir[0] == "/":
	out_dir = os.getcwd() + "/" + out_dir  

if not os.path.exists(out_dir):
	os.makedirs(out_dir)

title = "time-"
base_profile = "Plain"
profiles = ['Plain', 'Ghostery', 'Privacy Badger', 'AdBlock Plus', 'Disconnect', 'Blur', 'uBlock', 'Request Policy']
categories = ['Motors', 'E-Commerce', 'Forums', 'Games', 'Hobbies', 'News', 'Search Engines', 'Sport', 'Technology', 'Weather Forecast']
categories_palette = ["#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a"]
cookie_palette = ["#1f78b4", "#ff4c4c"] #"#b2df8a"
rotation = 45


# csv format
# "profile", "cookie", category", "run", "domain", "byte_index", "object_index", "on_load_time", "tot_bytes",
# "tot_objects", "tot_3rd_party", "unique_3rd_party", "tot_trackers", "unique_trackers", "trackers", "3rd_party"

df = pd.read_csv(open(csv_path))
df.replace(to_replace={'cookie': {'n': 'Without TP cookies setup', 'y': 'With TP cookies setup'}}, inplace=True)

#sns.set_context("paper")
sns.set(font_scale=1.7)
sns.set_style('ticks', {'font.family':'serif', 'font.serif':'Times New Roman'}) #sns.set_style("white")


# byte volume per profile, cookie; relative values
grouped = df.groupby(['profile', 'cookie']).mean().reset_index()
no_cookie_base = grouped[(grouped['profile']==base_profile) & (grouped['cookie']=='Without TP cookies setup')].on_load_time
cookie_base = grouped[(grouped['profile']==base_profile) & (grouped['cookie']=='With TP cookies setup')].on_load_time
grouped['With TP cookies setup'] = grouped[grouped['cookie']=='With TP cookies setup'].on_load_time.divide(cookie_base, fill_value=cookie_base)
grouped['Without TP cookies setup'] = grouped[grouped['cookie']=='Without TP cookies setup'].on_load_time.divide(no_cookie_base, fill_value=no_cookie_base)
grouped['rel_on_load_time']=grouped[['Without TP cookies setup', 'With TP cookies setup']].fillna(0).sum(axis=1).multiply(100, fill_value=100).subtract(100, fill_value=100)

ordered_profiles = list(grouped[grouped.cookie=='With TP cookies setup'].sort_values(by='rel_on_load_time', ascending=False).profile)
ordered_profiles = [p for p in ordered_profiles if p != base_profile]

# barplot
bar_rel_cookie = sns.factorplot(kind="bar",
				   x='profile', y='rel_on_load_time', hue='cookie',
                   hue_order=['Without TP cookies setup', 'With TP cookies setup'],
                   order=ordered_profiles,
                   data=grouped[grouped['profile'] != base_profile],
                   palette=cookie_palette, #"muted"
                   size=6,            			      
			   	   aspect=1.75,      			
			   	   legend_out=False)  		
bar_rel_cookie.despine(offset=10, trim=False)
bar_rel_cookie.set_xticklabels(ordered_profiles, rotation=rotation, ha="right")
bar_rel_cookie.fig.get_axes()[0].legend(title= '',loc='best')
bar_rel_cookie.set_axis_labels('','Loading time (%)')


# byte volume per profile, category; relative values
grouped = df.groupby(['profile', 'category']).mean().reset_index()
cat_base = defaultdict(float)
for c in categories:
	cat_base[c] = grouped[(grouped['profile']==base_profile) & (grouped['category']==c)].on_load_time
	grouped[c] = grouped[grouped['category']==c].on_load_time.divide(cat_base[c], fill_value=cat_base[c])
grouped['rel_on_load_time']=grouped[categories].fillna(0).sum(axis=1).multiply(100, fill_value=100).subtract(100, fill_value=100)

# barplot
bar_rel_cat = sns.factorplot(kind="bar",
				   x='profile', y='rel_on_load_time', hue='category',
				   hue_order=categories,
				   order=ordered_profiles,
				   palette=categories_palette, #"muted"
                   data=grouped[grouped['profile'] != base_profile],
                   size=6,            			      
			   	   aspect=3.0,      			
			   	   legend_out=False)  		
bar_rel_cat.despine(offset=10, trim=False)
bar_rel_cat.set_xticklabels(ordered_profiles, rotation=rotation, ha="right")
bar_rel_cat.fig.get_axes()[0].legend(title= '', loc='best', ncol=3)
bar_rel_cat.set_axis_labels('','Loading time (%)')

#sns.plt.show()


# Save figures
bar_rel_cookie.savefig(os.path.join(out_dir, title + "cookie-rel.svg"), bbox_inches='tight')
bar_rel_cookie.savefig(os.path.join(out_dir, title + "cookie-rel.pdf"), bbox_inches='tight')

bar_rel_cat.savefig(os.path.join(out_dir, title + "category-rel.svg"), bbox_inches='tight')
bar_rel_cat.savefig(os.path.join(out_dir, title + "category-rel.pdf"), bbox_inches='tight')

#violin_abs_cookie.savefig(os.path.join(out_dir, title + "violin.svg"), bbox_inches='tight')
#violin_abs_cookie.savefig(os.path.join(out_dir, title + "violin.pdf"), bbox_inches='tight')