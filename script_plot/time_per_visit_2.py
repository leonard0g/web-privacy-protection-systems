#!/usr/bin/python3

import os
import sys
import argparse
import glob
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from collections import defaultdict
from pandas.tools.plotting import andrews_curves

parser = argparse.ArgumentParser(description='blablabla')
parser.add_argument('input', metavar='input', type=str, help='Input csv file.') 
parser.add_argument('output', metavar='output', type=str, help='Output folder.') 

args = parser.parse_args()
csv_path = os.path.abspath(args.input)
out_dir = os.path.abspath(args.output)

if not out_dir[0] == "/":
	out_dir = os.getcwd() + "/" + out_dir  

if not os.path.exists(out_dir):
	os.makedirs(out_dir)

title = "time_per_visit_rank"
base_profile = "Plain"
categories = ['Motors', 'E-Commerce', 'Forums', 'Games', 'Hobbies', 'News', 'Search Engines', 'Sport', 'Technology', 'Weather Forecast']
profiles_palette = {"AdBlock Plus":"#fb9a99", "AdBlocker Ultimate":"#a6cee3", "Blur":"#b2df8a", "Disconnect":"#33a02c",
					"Ghostery":"#1f78b4", "Plain":"#e31a1c", "Privacy Badger":"#fdbf6f", "Request Policy":"#ff7f00", "uBlock":"#cab2d6"}
keep = ['uBlock', 'Plain', 'Privacy Badger']
cookie_palette = ["#1f78b4", "#ff4c4c"] #"#b2df8a"
rotation = 45
cut = 100	# number of webpages to be showed on the x axis

#sns.set_context("paper")
sns.set(font_scale=1.5)
sns.set_style('ticks', {'font.family':'serif', 'font.serif':'Times New Roman'}) #sns.set_style("white")
whis=[5, 95] #whiskers length for boxplots

# csv format
# "profile", "cookie", category", "run", "domain", "byte_index", "object_index", "on_load_time", "tot_bytes",
# "tot_objects", "tot_3rd_party", "unique_3rd_party", "tot_trackers", "unique_trackers", "trackers", "3rd_party"

df = pd.read_csv(open(csv_path))
df = df[df['cookie']=='n']
df.replace(to_replace={'cookie': {'n': 'without TP cookies setup', 'y': 'with TP cookies setup'}}, inplace=True)

df2 = df.groupby(['profile', 'domain']).mean().reset_index().sort_values(by=['profile', 'on_load_time'], ascending=False)
domain_order = df2[df2.profile == base_profile].domain
# Create the dictionary that defines the order for sorting
sorterIndex = dict(zip(domain_order,range(len(domain_order))))
# Generate a rank column that will be used to sort
# the dataframe numerically
df2['rank'] = df2['domain'].map(sorterIndex)
#sys.exit()

df2 = df2[['profile', 'domain', 'on_load_time', 'rank']] #.sort_values(by=['profile', 'on_load_time'], ascending=False)


markers_on = {"AdBlock Plus":[[12], '<'],
			#"AdBlocker Ultimate":[list(range(1,8,2)), 'p'],
			"Blur":[[22], '*'],
			"Disconnect":[[5], 'o'],
			"Ghostery":[[3], 's'],
			"Plain":[[12], ''],
			"Privacy Badger":[[12], 'D'],
			"Request Policy":[[12], 'v'],
			"uBlock":[[3], '^']}

fig, ax = plt.subplots(figsize=(10, 6))
#plt.xscale('log')
#xmax = 9999999
labels = list()
order = list() 
for key, group in df2.groupby('profile'):
	if key in keep:
		labels.append(key)
		order.append(group.on_load_time.values[20]) # get y value at 20th website
		if key == base_profile:
			linewidth = 4
		else:
			linewidth = 2
		#if len(group.index) < xmax:
		#	xmax = len(group.index)
		g = group.sort(['rank'], ascending=[True])
		ax = g.plot(ax=ax,
				x='domain', y='on_load_time',
				#by='profile',
				kind='line',
				color=profiles_palette[key],
				linewidth=linewidth,
				#linestyle='--',
				#markevery=[2],
				#markevery=markers_on[key][0],
				#marker=markers_on[key][1],
				markersize=12,
				sharex=True,			
				)

# sort legend
handles, _ = ax.get_legend_handles_labels()
d = dict(zip(labels, order))
lab = sorted(d, key=d.get, reverse=True)
d = dict(zip(handles, order))
han = sorted(d, key=d.get, reverse=True)
ax.legend(han, lab, fontsize=16)
#ax.set(xlim=(0, min(cut, xmax)))
ax.set(xlim=(0,len(domain_order)))
#ax.set(ylim=(0,26))
ax.set_xticks(range(0,len(domain_order)+1,9))
ax.set_xticklabels(range(0,cut+1,10))
print(len(domain_order))
#ax.set_xticklabels(domain_order)
sns.despine(offset=10, trim=False)
ax.set_xlabel('Page rank')
ax.set_ylabel('Loading time [s]')
#ax.set_yscale('log')

#sns.plt.show()
#sys.exit()

fig.savefig(os.path.join(out_dir, title + ".svg"), bbox_inches='tight')
fig.savefig(os.path.join(out_dir, title + ".pdf"), bbox_inches='tight')