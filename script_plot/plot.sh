#!/bin/sh
IN=$1
OUT=$2 			#./round1

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters"
    echo "Usage: " $0 " input_csv out_path"
    exit
fi

for script in $(ls -f "." | grep -e ".py$" | grep -v "connection");
do
	echo $script;
    ./$script $IN $OUT;
done;

