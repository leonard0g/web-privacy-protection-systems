#!/usr/bin/python3

import os
import sys
import argparse
import glob
import pandas as pd
import seaborn as sns
from collections import defaultdict
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='blablabla')
parser.add_argument('input', metavar='input', type=str, help='Input csv file.') 
parser.add_argument('output', metavar='output', type=str, help='Output folder.') 

args = parser.parse_args()
csv_path = os.path.abspath(args.input)
out_dir = os.path.abspath(args.output)

if not out_dir[0] == "/":
	out_dir = os.getcwd() + "/" + out_dir  

if not os.path.exists(out_dir):
	os.makedirs(out_dir)

title = "tracker-"
base_profile = "Plain"
profiles = ['Plain', 'Ghostery', 'Privacy Badger', 'AdBlock Plus', 'Disconnect', 'Blur', 'uBlock', 'Request Policy']
categories = ['Motors', 'E-Commerce', 'Forums', 'Games', 'Hobbies', 'News', 'Search Engines', 'Sport', 'Technology', 'Weather Forecast']
categories_palette = ["#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a"]
cookie_palette = ["#1f78b4", "#ff4c4c"] #"#b2df8a"
rotation = 45


# csv format
# "profile", "cookie", category", "run", "domain", "byte_index", "object_index", "on_load_time", "tot_bytes",
# "tot_objects", "tot_3rd_party", "unique_3rd_party", "tot_trackers", "unique_trackers", "trackers", "3rd_party"

df = pd.read_csv(open(csv_path))
df.replace(to_replace={'cookie': {'n': 'Without TP cookies setup', 'y': 'With TP cookies setup'}}, inplace=True)

#sns.set_context("paper")
sns.set(font_scale=1.7)
sns.set_style('ticks', {'font.family':'serif', 'font.serif':'Times New Roman'}) #sns.set_style("white")

# total number of unique trackers, noPlugin profile
l = df[(df['profile']==base_profile) & (df['cookie']=='Without TP cookies setup')].trackers.tolist()
l = [str(y) for y in l]
l = [x.split(':')[0] for y in l for x in y.split(' ')]
base_no_cookie_trackers = len(set(l))

l = df[(df['profile']==base_profile) & (df['cookie']=='With TP cookies setup')].trackers.tolist()
l = [str(y) for y in l]
l = [x.split(':')[0] for y in l for x in y.split(' ')]
base_cookie_trackers = len(set(l))


tracker_abs = defaultdict(list)
tracker_perc = defaultdict(list)

for profile in df.profile.unique():
	l = df[(df['profile']==profile) & (df['cookie']=='Without TP cookies setup')].trackers.tolist()
	l = [str(y) for y in l]
	l = [x.split(':')[0] for y in l for x in y.split(' ')]
	no_cookie_trackers = len(set(l))

	l = df[(df['profile']==profile) & (df['cookie']=='With TP cookies setup')].trackers.tolist()
	l = [str(y) for y in l]
	l = [x.split(':')[0] for y in l for x in y.split(' ')]
	cookie_trackers = len(set(l))

	tracker_abs['profile'].append(profile)
	tracker_abs['cookie'].append('With TP cookies setup')
	tracker_abs['count'].append(base_cookie_trackers - cookie_trackers)

	tracker_abs['profile'].append(profile)
	tracker_abs['cookie'].append('Without TP cookies setup')
	tracker_abs['count'].append(base_no_cookie_trackers - no_cookie_trackers)

	tracker_perc['profile'].append(profile)
	tracker_perc['cookie'].append('With TP cookies setup')
	tracker_perc['perc'].append((1.0 - cookie_trackers/base_cookie_trackers) * 100.0)

	tracker_perc['profile'].append(profile)
	tracker_perc['cookie'].append('Without TP cookies setup')
	tracker_perc['perc'].append((1.0 - no_cookie_trackers/base_no_cookie_trackers) * 100.0)

df_tr_perc = pd.DataFrame.from_dict(tracker_perc)
df_tr_abs = pd.DataFrame.from_dict(tracker_abs)

ordered_profiles = list(df_tr_perc.groupby(['profile']).mean().reset_index().sort_values(by='perc', ascending=False).profile)
ordered_profiles = [p for p in ordered_profiles if p != base_profile]
ordered_profiles.insert(0, base_profile)

# barplot tracker
bar_tr_rel = sns.factorplot(kind="bar",
				   x = 'profile', y = 'perc', hue='cookie',
                   data=df_tr_perc[df_tr_perc['profile'] != base_profile],
                   hue_order=['Without TP cookies setup', 'With TP cookies setup'],
                   order=ordered_profiles[1:],
                   palette=cookie_palette, #"muted"
                   size=6,            			      
			   	   aspect=1.75,      			
			   	   legend_out=False)  		
bar_tr_rel.despine(offset=10, trim=False)
bar_tr_rel.set_xticklabels(ordered_profiles[1:], rotation=rotation, ha="right")
bar_tr_rel.fig.get_axes()[0].legend(title= '',loc='best')
bar_tr_rel.set_axis_labels('','Unique trackers reduction (%)')
bar_tr_rel.set(ylim=(0,100))


ordered_profiles = list(df_tr_abs.groupby(['profile']).mean().reset_index().sort_values(by='count', ascending=False).profile)
ordered_profiles = [p for p in ordered_profiles if p != base_profile]
ordered_profiles.insert(0, base_profile)

bar_tr_abs = sns.factorplot(kind="bar",
				   x = 'profile', y = 'count', hue='cookie',
                   data=df_tr_abs[df_tr_abs['profile'] != base_profile],
                   hue_order=['Without TP cookies setup', 'With TP cookies setup'],
                   order=ordered_profiles[1:],
                   palette=cookie_palette, #"muted"
                   size=6,            			      
			   	   aspect=1.75,      			
			   	   legend_out=False)  		
bar_tr_abs.despine(offset=10, trim=False)
bar_tr_abs.set_xticklabels(ordered_profiles[1:], rotation=rotation, ha="right")
bar_tr_abs.fig.get_axes()[0].legend(title= '',loc='best')
bar_tr_abs.set_axis_labels('','Unique trackers reduction (abs)')
bar_tr_abs.set(ylim=(0,None))

#sns.plt.show()

# Save figures
bar_tr_rel.savefig(os.path.join(out_dir, title + "cookie-rel.svg"), bbox_inches='tight')
bar_tr_rel.savefig(os.path.join(out_dir, title + "cookie-rel.pdf"), bbox_inches='tight')

bar_tr_abs.savefig(os.path.join(out_dir, title + "cookie-abs.svg"), bbox_inches='tight')
bar_tr_abs.savefig(os.path.join(out_dir, title + "cookie-abs.pdf"), bbox_inches='tight')

#violin_abs_cookie.savefig(os.path.join(out_dir, title + "violin.svg"), bbox_inches='tight')
#violin_abs_cookie.savefig(os.path.join(out_dir, title + "violin.pdf"), bbox_inches='tight')