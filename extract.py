#!/usr/bin/python3

import argparse
import os
import re
import glob
import sys
import csv
import json
from dateutil import parser
import operator
import datetime
from collections import Counter
import tldextract
import multiprocessing

OUT_CSV = "extract_out.csv"

tracker_list = list()
tracker_cache = list()
in_dir = ""
out_dir = ""

def main():
	global tracker_list
	global in_dir
	global out_dir

	parser = argparse.ArgumentParser(description='blablabla')
	parser.add_argument('-i', '--input', metavar='input', type=str, required=True, help='Input folder.') 
	parser.add_argument('-o', '--output', metavar='output', type=str, required=True, help='Output path.') 
	parser.add_argument('-t', '--tracker_list', metavar='tracker_list', type=str, required=True, help='Tracker list path.') 

	args = parser.parse_args()
	in_dir = os.path.abspath(args.input)
	out_dir = os.path.abspath(args.output)
	tracker_file = os.path.abspath(args.tracker_list)

	if not out_dir[0] == "/":
		out_dir = os.getcwd() + "/" + out_dir  
	
	if not os.path.exists(out_dir):
		os.makedirs(out_dir)
	
	# build tracker list from file
	with open(tracker_file, 'r') as f:
		for line in f:
			tracker_list.append(line.strip())

	jobs = []
	for profile in os.listdir(in_dir):
		print("Starting process for {}".format(profile))
		j = multiprocessing.Process(target=do_it, args=(in_dir, profile, ), name=profile)
		jobs.append(j)
		j.start()

	for j in jobs:
		j.join()
		print("{}.exitcode = {}".format(j.name, j.exitcode))


def do_it(in_dir, profile):
	global OUT_CSV	
	
	with open(os.path.join(out_dir, "_".join([profile, OUT_CSV])), 'w') as csvfile:
		spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
		# write header
		spamwriter.writerow(["profile", "category", "run", "domain", "byte_index", "object_index", "on_load_time", "tot_bytes", "tot_objects", "tot_3rd_party", "unique_3rd_party", "tot_trackers", "unique_trackers", "trackers", "3rd_party"])
		for root, _, _ in os.walk(os.path.join(in_dir, profile), topdown=False):
			print(root)
			try:
				roundn, category = root.split("/")[-1].split("_")[0:2]
			except Exception as e:
				continue
			#print(profile, roundn, category)

			for _, _, files in os.walk(root, topdown=False):
				pass
			for f in files:
				#print(f)
				if re.search(r'Archive.*', f):
					# skip fucked up hars
					continue

				# extract statistics from har file
				row = [profile, category, int(roundn)] + extract_index(os.path.join(root, f))
				#print(row)
				spamwriter.writerow(row)



def extract_index(har_path):
	input_har = json.load(open(har_path, "r"))

	objects=[]
	bytes=[]
	tot_bytes=0
	tot_obj=0
	third_party = Counter()
	trackers = Counter()

	start_time=parser.parse(input_har["log"]["pages"][0]["startedDateTime"]).timestamp()
	domain=input_har["log"]["pages"][0]["title"].split("/")[2]
	#on_load_time=input_har["log"]["pages"][0]["pageTimings"]["onLoad"]
	on_load_time=input_har["log"]["pages"][0]["pageTimings"]["onContentLoad"]

	for entry in input_har["log"]["entries"]:

		obj_start_time=parser.parse(entry["startedDateTime"]).timestamp()
		obj_delta_time= sum( [ v for v in entry["timings"].values() if v != -1])
		obj_time = obj_start_time - start_time + obj_delta_time/1000
		size = entry["response"]["bodySize"]
		objects.append( (obj_time, 1   ))
		bytes.append  ( (obj_time, size))
		tot_bytes+=size
		tot_obj+=1
		last_obj_time=obj_time
		host = entry["request"]["headers"][0]["value"]

		#d = tldextract.extract(host).domain
		d = ".".join(list(tldextract.extract(host)))
		if not re.search(d, domain):
			third_party[d] += 1
			t = is_tracker(host, trackers.keys())
			if t:
				trackers[d] += 1


	'''
	#should return 4
	tot_bytes=tot_obj=4
	bytes=( (1,1),(3,1),(4,1),(8,1) ) 
	objects=( (1,1),(3,1),(4,1),(8,1) )

	objects=sorted(objects,key=operator.itemgetter(0))
	bytes=sorted(bytes,key=operator.itemgetter(0))
	'''

	cumul_bytes=0
	cumul_objects=0
	for i in range (len(bytes)):
		time, size = bytes[i]
		bytes[i]   = (time, size/tot_bytes + cumul_bytes)
		objects[i] = (time, 1/tot_obj + cumul_objects)
		cumul_bytes=size/tot_bytes + cumul_bytes
		cumul_objects=1/tot_obj + cumul_objects

	bytes = [(t, round(1-b,5)) for t,b in bytes]
	objects = [(t, round(1-b,5)) for t,b in objects]


	prec_score=1
	prec_time=0
	byte_index=0

	for time, score in bytes:
		component=(time-prec_time)*prec_score
		byte_index+=component
		prec_score=score
		prec_time = time

	prec_score=1
	prec_time=0
	object_index=0

	for time, score in objects:
		component=(time-prec_time)*prec_score
		object_index+=component
		prec_score=score
		prec_time = time

	tt = ""
	for k, v in trackers.items():
		tt += k + ":" + str(v) + " "

	tp = ""
	for k, v in third_party.items():
		tp += k + ":" + str(v) + " "


	#print(domain, byte_index, object_index, on_load_time/1000, DOM_load_time/1000 )
	return [domain, byte_index, object_index, on_load_time, tot_bytes, tot_obj, sum(third_party.values()), len(third_party), sum(trackers.values()), len(trackers), tt, tp]


def is_tracker(host, trackers_map_keys):
	global tracker_list
	global tracker_cache

	if host in trackers_map_keys or host in tracker_cache:
		return host

	if tracker_list is None:
		return None
	for d in tracker_list:
		#print(host, d)
		m = re.search(d, host)
		if m: 
			tracker_cache.append(d)
			return d
	return None

			

if __name__ == "__main__":
	main()




